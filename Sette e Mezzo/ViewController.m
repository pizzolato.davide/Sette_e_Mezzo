//
//  ViewController.m
//  Sette e mezzo
//
//  Created by DavideMac on 09/11/16.
//  Copyright © 2016 Davide Pizzolato. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

bool cartaUscita[5][11];
int cartaCopertaBanco;
int cartaRnd;
UIImageView *img;
int cartaBanco; int cartaGiocatore;
float puntiBanco; float puntiBancoTot; float puntiGiocatore;
int vinte = 0, perse = 0;


- (void)viewDidLoad {
    [super viewDidLoad];
    [self mescolaCarte];
    [self start];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (void)start {
    cartaBanco = 102;
    cartaGiocatore = 201;
    puntiBanco = 0;
    cartaCopertaBanco = [self cartaRnd];
    puntiBancoTot = [self valoreCarta:cartaCopertaBanco];
    puntiGiocatore = [self stampaCarta:cartaGiocatore++];
    [self aggiornaPunti];
    [self ia];
}

- (IBAction)carta:(id)sender {
    puntiGiocatore += [self stampaCarta:cartaGiocatore++];
    [self aggiornaPunti];
    if (puntiGiocatore > 7.5)
    {
        [self stop:nil];
    }
    if (puntiGiocatore == 7.5)
    {
        [self stop:nil];
    }
}

- (IBAction)stop:(id)sender {
    img = (UIImageView *)[self.view viewWithTag:101];
    [img setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%d_%d", cartaCopertaBanco/100, cartaCopertaBanco%100]]];
    [_puntiBanco setText:[NSString stringWithFormat:@"%.01f", puntiBancoTot]];
    [_stopButton setHidden:YES];
    [_cartaButton setHidden:YES];
    [_rigiocaButton setHidden:NO];
    NSString *imgWinLose;
    if (puntiBancoTot > 7.5)
    {
        imgWinLose = @"win";
        vinte++;
    }
    else
    {
        if (puntiGiocatore > 7.5)
        {
            imgWinLose = @"lose";
            perse++;
        }
        else
        {
            if (puntiGiocatore > puntiBancoTot)
            {
                imgWinLose = @"win";
                vinte++;
            }
            else
            {
                if (puntiGiocatore < puntiBancoTot)
                {
                    imgWinLose = @"lose";
                    perse++;
                }
                else
                {
                    NSLog(@"Pareggio");
                }
            }
        }
    }
    [_vinte setText:[NSString stringWithFormat:@"%d", vinte]];
    [_perse setText:[NSString stringWithFormat:@"%d", perse]];
    [_winLose setImage:[UIImage imageNamed:imgWinLose]];
}

- (IBAction)rigioca:(id)sender {
    [_rigiocaButton setHidden:YES];
    [_cartaButton setHidden:NO];
    [_stopButton setHidden:NO];
    [_winLose setImage:nil];
    [self mescolaCarte];
    img = (UIImageView *)[self.view viewWithTag:101];
    [img setImage:[UIImage imageNamed:@"retro"]];
    for (int i = 102; i < 214; i++) {
        img = (UIImageView *)[self.view viewWithTag:i];
        [img setImage:nil];
        if (i == 114)
            i = 201;
    }
    [self start];
}

-(float)stampaCarta:(int)tag {
    int cartaRnd = [self cartaRnd];
    img = (UIImageView *)[self.view viewWithTag:tag];
    [img setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%d_%d", cartaRnd/100, cartaRnd%100]]];
    float valoreCarta = [self valoreCarta:cartaRnd];
    return valoreCarta;
}

-(float)valoreCarta:(int)cartaRnd {
    cartaRnd %= 100;
    if (cartaRnd>7)
    {
        return 0.5;
    }
    else
    {
        return cartaRnd;
    }
}

-(int)cartaRnd {
    int semeRnd = arc4random()%4 + 1;
    int numRnd = arc4random()%10 + 1;
    while (cartaUscita[semeRnd][numRnd])
    {
        if (numRnd == 10)
        {
            if (semeRnd == 4)
            {
                semeRnd = 1;
            }
            else
            {
                semeRnd++;
            }
        }
        else
        {
            numRnd++;
        }
    }
    cartaUscita[semeRnd][numRnd] = 1;
    cartaRnd = semeRnd * 100 + numRnd;
    return cartaRnd;
}

-(void)aggiornaPunti {
    [_puntiBanco setText:[NSString stringWithFormat:@"%.01f + ?", puntiBanco]];
    [_puntiGiocatore setText:[NSString stringWithFormat:@"%.01f", puntiGiocatore]];
}

-(void)ia {
    bool continua = true;
    int percent = 100;
    int puntiIA;
    while (continua)
    {
        puntiIA = puntiBancoTot * 10;
        switch (puntiIA)
        {
            case 0:
                percent = 100;
                break;
            case 5:
                percent = 100;
                break;
            case 10:
                percent = 100;
                break;
            case 15:
                percent = 99;
                break;
            case 20:
                percent = 97;
                break;
            case 25:
                percent = 95;
                break;
            case 30:
                percent = 90;
                break;
            case 35:
                percent = 85;
                break;
            case 40:
                percent = 55;
                break;
            case 45:
                percent = 50;
                break;
            case 50:
                percent = 20;
                break;
            case 55:
                percent = 15;
                break;
            case 60:
                percent = 10;
                break;
            case 65:
                percent = 5;
                break;
            case 70:
                percent = 1;
                break;
            case 75:
                percent = 0;
                break;
        }
        int mossa = arc4random() % 100 + 1;
        if (puntiBancoTot == 7.5)
        {
            [_puntiBanco setText:@"7.5"];
            img = (UIImageView *)[self.view viewWithTag:101];
            [img setImage:[UIImage imageNamed:[NSString stringWithFormat:@"%d_%d", cartaCopertaBanco/100, cartaCopertaBanco%100]]];
            continua = false;
            [_cartaButton setHidden:NO];
            [_stopButton setHidden:NO];
        }
        else
        {
            if (puntiIA > 75)
            {
                [self stop:nil];
                continua = false;
            }
            else
            {
                if (mossa <= percent)
                {
                    float punti = [self stampaCarta:cartaBanco++];
                    puntiBanco += punti;
                    puntiBancoTot += punti;
                    
                }
                else
                {
                    continua = false;
                    [_cartaButton setHidden:NO];
                    [_stopButton setHidden:NO];
                }
                [self aggiornaPunti];
            }
        }
    }
}

-(void)mescolaCarte {
    for (int seme = 1; seme<5; seme++)
    {
        for (int num = 1; num<11; num++)
        {
            cartaUscita[seme][num] = 0;
        }
    }
}

@end
